import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ImageService} from "../service/image.service";

export class Image {
  public id: any = '';
  public title: string = '';
  public description: string = '';
  public category: string = '';
  public url: string = '';
}

@Component({
  selector: 'app-add-update-image',
  templateUrl: './add-update-image.component.html',
  styleUrls: ['./add-update-image.component.css'],
  providers: [ImageService]
})
export class AddUpdateImageComponent implements OnInit {

  model = new Image();

  constructor(private activatedroute: ActivatedRoute, private imageService: ImageService, private router: Router) {
  }

  ngOnInit() {
    this.activatedroute.params.subscribe(data => {
      if (data) {
        this.model.id = data.id;
        this.model.title = data.title;
        this.model.description = data.description;
        this.model.category = data.category;
        this.model.url = data.url;
      }
    })
  }

  onSubmit(form) {
    if (this.model.id) {
      this.imageService.update(this.model.id, form.value).subscribe(res => {
          //console.log(res);
          this.router.navigate(['/image/list']);
        }, error => {
          console.log('Error Update: ' + error.message)
        }
      )
    } else {
      this.imageService.add(form.value).subscribe(res => {
        //console.log(res);
        this.router.navigate(['/image/list']);
        }, error => {
          console.log('Error Post: ' + error.message)
        }
      )
    }

  }

}
