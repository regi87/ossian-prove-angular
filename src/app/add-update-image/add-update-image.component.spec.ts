import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpdateImageComponent } from './modal-add-update-image.component';

describe('ModalAddUpdateImageComponent', () => {
  let component: AddUpdateImageComponent;
  let fixture: ComponentFixture<AddUpdateImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUpdateImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpdateImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
