import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Ossian Prove Angular';

  constructor(private router: Router) {}

  ngOnInit(){
  }

  logout(){
    if(localStorage.getItem('email')){
      localStorage.removeItem('email');
      this.router.navigate(['']);
    }
  }
}
