import { Component, OnInit } from '@angular/core';
import {UserService} from "../service/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  model= {
    email : '',
    password: '',
    confirmPassword: ''
  }
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(form) {
    this.userService.register(form.value).subscribe(res => {
      this.router.navigate(['login']);
      }, error => {
        console.log('Error Post: ' + error.message)
      }
    )
  }
}
