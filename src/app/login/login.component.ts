import { Component, OnInit } from '@angular/core';
import {UserService} from "../service/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model= {
    email : '',
    password: '',
  }
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(form) {
    this.userService.login(form.value).subscribe(res => {
      if(res.result){
        localStorage.setItem('email', this.model.email)
        this.router.navigate(['image/list']);
      }
      }, error => {
        console.log('Error Post: ' + error.message)
      }
    )
  }

}
