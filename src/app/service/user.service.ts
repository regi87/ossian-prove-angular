import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  API_URL: string = 'https://localhost:8000/api/user';

  constructor(private http: HttpClient) { }

  register(data):Observable<any>{
    return this.http.post(this.API_URL + '/register', data);
  }

  login(data):Observable<any>{
    return this.http.post(this.API_URL + '/login', data);
  }
}
