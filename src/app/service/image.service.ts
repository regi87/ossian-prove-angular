import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class ImageService {

  API_URL: string = 'https://localhost:8000/api/image';

  constructor(private http: HttpClient) {
  }

  add(data):Observable<any>{
    return this.http.post(this.API_URL + '/add', data);
  }

  addMultiple(data):Observable<any>{
    return this.http.post(this.API_URL + '/add-multiple', data);
  }

  list():Observable<any>{
    let httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };

    return this.http.get(this.API_URL + '/list', httpOptions);
  }

  delete(id):Observable<any>{
    return this.http.delete(this.API_URL + '/delete/' + id);
  }

  update(id, data):Observable<any>{
    return this.http.put(this.API_URL + '/update/' + id, data);
  }

  get(id):Observable<any>{
   return this.http.get(this.API_URL + '/get/' + id) ;
  }
}
