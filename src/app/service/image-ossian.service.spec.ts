import { TestBed } from '@angular/core/testing';

import { ImageOssianService } from './image-ossian.service';

describe('ImageOssianService', () => {
  let service: ImageOssianService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImageOssianService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
