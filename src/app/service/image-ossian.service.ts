import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ImageOssianService {

  API_URL: string = 'http://internal.ossian.tech/api/Sample';

  constructor(private http: HttpClient) {
  }

  list():Observable<any>{
    return this.http.get(this.API_URL);
  }
}
