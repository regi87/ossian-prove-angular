import {Component, OnInit} from '@angular/core';
import {ImageService} from "../service/image.service";
import {Router} from "@angular/router";
import {ImageOssianService} from "../service/image-ossian.service";

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css'],
  providers: [ImageService, ImageOssianService]
})
export class ImageComponent implements OnInit {

  images = [];

  /**
   *
   * @param imageService
   * @param imageOssianService
   * @param router
   */
  constructor(private imageService: ImageService, private imageOssianService: ImageOssianService, private router: Router) {
  }

  ngOnInit(): void {
    this.imageService.list().subscribe(res => {
        this.images = res;
      }, error => {
        console.log('ERROR : ' + error.message);
      }
    );
  }

  /**
   * Object
   * @param data
   */
  updateImage(data): void {
    this.router.navigate(['/image/add-update', data]);
  }

  /**
   * Integer
   * @param id
   */
  deleteImage(id): void {
    this.imageService.delete(id).subscribe(res => {
      //console.log(res);
      this.imageService.list().subscribe(res => {
          this.images = res;
        }, error => {
          console.log('ERROR : ' + error.message);
        }
      );
    }, error => {
      console.log('Error Delete: ' + error);
    })
  }

  getImageFromOssianDatabase() {
    this.imageOssianService.list().subscribe(res => {
        console.log(res)
        this.imageService.addMultiple(res).subscribe(res => {
            console.log(res);
            this.imageService.list().subscribe(res => {
                this.images = res;
              }, error => {
                console.log('ERROR : ' + error.message);
              }
            );
          }, error => {
            console.log('Error Post: ' + error.message)
          }
        )
      }, error => {
        console.log('ERROR : ' + error.message);
      }
    );
  }

}
